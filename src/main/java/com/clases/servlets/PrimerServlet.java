package com.clases.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PrimerServlet extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest peticion,
					  HttpServletResponse respuesta)throws ServletException, IOException{
		PrintWriter salida= respuesta.getWriter();
		salida.println("hola mundo");
	}
}
