package com.clases.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.tools.GeneradorHTML;

@WebServlet("/tercero")
public class HTMLServletConGenerador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public HTMLServletConGenerador() {
        super();
        
    }
	
	public void doGet(HttpServletRequest peticion, 
					HttpServletResponse respuesta)
		throws ServletException, IOException{
		
		respuesta.setContentType("text/html");
		PrintWriter salida=respuesta.getWriter();
		salida.println(GeneradorHTML.generar());
	}
}
